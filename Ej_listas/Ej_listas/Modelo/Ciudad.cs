﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Ej_listas
{
    public class Ciudad
    {
        public string Nombre { get; set; }
        public int Poblacion { get; set; }
        public ImageSource Source { get; set; }
    }
}
