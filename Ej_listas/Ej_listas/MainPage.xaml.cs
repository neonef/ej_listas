﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Ej_listas
{
	public partial class MainPage : ContentPage
	{
        private ListView listView;

		public MainPage()
		{
			InitializeComponent();

            listView = new ListView
            {
                RowHeight = 40
            };
            listView.ItemsSource = new Ciudad[]
            {
                new Ciudad { Nombre = "Almería", Poblacion = 194515, Source = ImageSource.FromUri(new Uri("https://d1bvpoagx8hqbg.cloudfront.net/originals/experiencia-erasmus-almeria-espana-ana-895ccc2edddf6abb607863c429819a15.jpg")) },
                new Ciudad { Nombre = "Cádiz", Poblacion = 118919, Source = ImageSource.FromUri(new Uri("http://cdni.condenast.co.uk/646x430/a_c/Cadiz_CNT_16Aug10_rex_b.jpg")) },
                new Ciudad { Nombre = "Córdoba", Poblacion = 326609},
                new Ciudad { Nombre = "Granada", Poblacion = 234758},
                new Ciudad { Nombre = "Huelva", Poblacion = 145468},
                new Ciudad { Nombre = "Jaén", Poblacion = 114658},
                new Ciudad { Nombre = "Málaga", Poblacion = 569009, Source = ImageSource.FromUri(new Uri("http://www.cheapflightslab.com/wp-content/uploads/2017/05/Background-Explora-Malaga-ayuntamiento.jpg")) },
                new Ciudad { Nombre = "Sevilla", Poblacion = 690566},
            };

            listView.ItemTemplate = new DataTemplate(typeof(TextCell));
            listView.ItemTemplate.SetBinding(TextCell.TextProperty, "Nombre");
            listView.ItemTemplate.SetBinding(TextCell.DetailProperty, "Poblacion");

            listView.ItemSelected += OnItemSelected;

            (Content as StackLayout).Children.Add(listView);
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (listView.SelectedItem != null)
            {
                var detalleCiudadPage = new DetalleCiudadPage();
                detalleCiudadPage.BindingContext = e.SelectedItem as Ciudad;
                listView.SelectedItem = null;
                await Navigation.PushModalAsync(detalleCiudadPage);
            }
        }
    }
}
